package jsonpretty

import (
	"bytes"
	"encoding/json"
)

// Print Gets indented json files.
func Print(data []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, data, "", "  ")
	return out.Bytes(), err
}
