package jsonpretty

import (
	"testing"
)

func TestPrettyFormat(t *testing.T) {
	in := []byte(`{"test":"data","some":[]}`)
	expected := []byte(
		`{
  "test": "data",
  "some": []
}`)
	out, err := Print(in)

	if err != nil {
		t.Fatal(err)
	}
	if len(out) != len(expected) {
		t.Fatal("wrong output lenght", len(out), len(expected))
	}
	for i := range out {
		if out[i] != expected[i] {
			t.Fatal("wrong output", out[i], expected[i])
		}
	}

}

// func TestAPrettyFormat(t *testing.T) {
// 	in := []byte(`{"test": [{"a":1,"b":2}]}`)
// 	expected := []byte(
// 		`{
//   "test": [
//   {
//     "a": 1,
//     "b": 2
//   }
//   ]
// }`)
// 	out, err := Print(in)

// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if len(out) != len(expected) {
// 		t.Fatal("wrong output lenght", len(out), len(expected))
// 	}
// 	for i := range out {
// 		if out[i] != expected[i] {
// 			t.Fatal("wrong output", out[i], expected[i])
// 		}
// 	}

// }
