// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"cli/mycli/acli/types"
	"fmt"
	"go-json-pretty-print/src/filehandler"

	"github.com/spf13/cobra"
)

var local bool
var remote bool

var ppCmd = &cobra.Command{
	Use:   "pp",
	Short: "Pretty prints a json file",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		commandHandler(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(ppCmd)
	ppCmd.Flags().BoolP("remote", "r", false, "Pretty print a remote json file")
	ppCmd.Flags().BoolP("local", "l", false, "Pretty print a local json file")
}

func commandHandler(cmd *cobra.Command, args []string) {
	remote, err := cmd.Flags().GetBool("remote")
	local, err := cmd.Flags().GetBool("local")
	if err != nil {
		fmt.Println(err)
		return
	}
	switch desitionTree(remote, local) {
	case types.RemoteFile:
		remoteFile(args[0])
	case types.LocalFile:
		localFile(args[0])
	default:
		filterFile(args[0])
	}
}

func desitionTree(remote bool, local bool) types.FlagFileType {
	if remote == local {
		return types.Unknown
	}
	if remote {
		return types.RemoteFile
	}
	if local {
		return types.LocalFile
	}
	return types.Unknown
}

func remoteFile(url string) {
	filehandler.Handle(url)
}

func localFile(url string) {
	filehandler.Handle(url)
}

func filterFile(url string) {
	result, err := filehandler.Handle(url)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%s\n", result)
}
