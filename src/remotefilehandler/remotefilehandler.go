package remotefilehandler

import (
	"io/ioutil"
	"net/http"
)

// Handle gets data from a remote file.
func Handle(arg string) []byte {
	client := http.Client{}
	url := arg
	request, err := http.NewRequest("GET", url, nil)
	resp, err := client.Do(request)
	check(err)
	body, err := ioutil.ReadAll(resp.Body)
	return body
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
