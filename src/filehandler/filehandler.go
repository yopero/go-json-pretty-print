package filehandler

import (
	"errors"
	"go-json-pretty-print/src/jsonpretty"
	"go-json-pretty-print/src/localfilehandler"
	"go-json-pretty-print/src/remotefilehandler"
	"go-json-pretty-print/src/types"
	"net/url"
	"os"
)

// Handle Handles the file passed to the tool.
func Handle(arg string) ([]byte, error) {
	fileType, fileData := getFileInfo(arg)
	data, err := processFile(fileType, fileData)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func processFile(fileType types.TypeOfFile, fileData []byte) ([]byte, error) {
	switch fileType {
	case types.IsURL:
		data, e := jsonpretty.Print(fileData)
		if data == nil {
			return processFile(types.IsInvalidURL, data)
		}
		if e != nil {
			return processFile(types.IsBadJSONFormat, nil)
		}
		return data, nil

	case types.IsLocalFile:
		data, e := jsonpretty.Print(fileData)
		if e != nil {
			return processFile(types.IsBadJSONFormat, nil)
		}
		return data, nil
	case types.IsDir:
		return nil, errors.New(`"gson" does not do dirs`)
	case types.IsInvalidLocalFile:
		return nil, errors.New("Invalid Local File")
	case types.IsInvalidURL:
		return nil, errors.New("Invalid URL")
	case types.IsBadJSONFormat:
		return nil, errors.New("Bad JSON Format")
	}
	return nil, nil
}

func getFileInfo(arg string) (types.TypeOfFile, []byte) {
	fileInfo, err := os.Stat(arg)
	if err != nil {
		if isValidURL(arg) {
			return getDataFromRemoteFile(arg)
		}
		return types.IsInvalidLocalFile, nil
	}
	return getDataFromLocalFile(fileInfo, arg)
}

func getDataFromRemoteFile(arg string) (types.TypeOfFile, []byte) {
	return types.IsURL, remotefilehandler.Handle(arg)
}

func getDataFromLocalFile(fileInfo os.FileInfo, arg string) (types.TypeOfFile, []byte) {
	switch mode := fileInfo.Mode(); {
	case mode.IsRegular():
		return types.IsLocalFile, localfilehandler.Handle(arg)
	case mode.IsDir():
		return types.IsDir, nil
	default:
		return types.IsInvalidLocalFile, nil
	}
}

func isValidURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}
	return true
}
