package filehandler

import (
	"errors"
	"testing"
)

func TestBadJson(t *testing.T) {
	_, err := Handle("../testdata/bad.json")
	var errA = errors.New("Bad JSON Format")
	if err.Error() != errA.Error() {
		t.Fatal("Wrong type of error")
	}
}

func TestGoodJson(t *testing.T) {
	result, err := Handle("../testdata/good.json")
	if err != nil {
		t.Fatal(result)
	}
}

func TestNonExistentJson(t *testing.T) {
	_, err := Handle("../testdata/xyz.json")
	var errA = errors.New("Invalid Local File")
	if err.Error() != errA.Error() {
		t.Fatal("Wrong type of error")
	}
}

func TestGoodRemoteJson(t *testing.T) {
	result, err := Handle("https://gist.githubusercontent.com/moredip/4165313/raw/ad00ee5bc70016a70976736d5cdf0463d5f7ea15/test.json")
	if err != nil {
		t.Fatal(result)
	}
}
func TestBadRemoteJson(t *testing.T) {
	_, err := Handle("https://xyzgist.githubusercontent.com/moredip/4165313/raw/ad00ee5bc70016a70976736d5cdf0463d5f7ea15/test.json")
	var errA = errors.New("Invalid URL")
	if err.Error() != errA.Error() {
		t.Fatal("Wrong type of error")
	}
}
func TestBadFormatRemoteJson(t *testing.T) {
	_, err := Handle("https://gist.githubusercontent.com/danie16arrido/e895acc4524f3e085cee185b2ad71083/raw/7cdcd41b177ed95a3141640e328489221d937024/badRemoteJson.json")
	var errA = errors.New("Bad JSON Format")
	if err.Error() != errA.Error() {

		t.Fatal("Wrong type of error")
	}
}
