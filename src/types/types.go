package types

type TypeOfFile int

const (
	IsInvalidLocalFile = iota
	IsLocalFile
	IsDir
	IsURL
	IsInvalidURL
	IsBadJSONFormat
)
