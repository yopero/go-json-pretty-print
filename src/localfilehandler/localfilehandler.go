package localfilehandler

import (
	"io/ioutil"
)

// Handle Gets data from a local file.
func Handle(arg string) []byte {
	data, err := ioutil.ReadFile(arg)
	check(err)
	return data
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
